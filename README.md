# ACM

## INSTALL

1. install nodejs and npm (https://nodejs.org/en/)
1. run `npm i`
1. download the GeoLite2 City database from https://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz, unzip and place the `.mmdb` file in `./GeoLite2DB/` as `GeoLite2-City.mmdb` (the path can be changed in `config.js`)
1. run with `node index.js`
1. edit the `config.js` file to change the mongodb url and database name

module.exports = {
  mongodb: {
    url: "mongodb://localhost:27017/",
    dbName: "db"
  },
  geoLite2DB: "./GeoLite2DB/GeoLite2-City.mmdb"
};

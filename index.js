const express = require("express");
const maxmind = require("maxmind");
const db = require("./mongoClient")();
const ipRangeCheck = require("ip-range-check");
const httpStatus = require("http-status-codes");
const schemas = require("./schemas");
const { geoLite2DB } = require("./config");
const { collectionName, supportedContextTypes, port } = require("./consts");

const app = express();

const getCollection = async () => (await db).collection(collectionName);

const lookup = maxmind.open(geoLite2DB);

app.use(express.json());

app.get("/resources/:name", async (req, res) => {
  const { name } = req.params;
  const { ip } = req.query;
  const contextType = req.headers["context-type"];

  if (!supportedContextTypes.includes(contextType)) {
    res.status(httpStatus.BAD_REQUEST);
    res.json("unsupported context type");
    return;
  }
  const collection = await getCollection();
  const contextWrapper = await collection.findOne({ name });

  if (!contextWrapper) {
    res.status(httpStatus.NOT_FOUND);
    res.json("context not found");
    return;
  }

  const ipAndLocationAllowed = await ipAndLocationAllowedForContext(
    ip,
    contextWrapper
  );

  if (!ipAndLocationAllowed) {
    res.status(httpStatus.FORBIDDEN);
    res.json("access not allowed");
    return;
  }

  res.json(contextWrapper.context);
});

app.post("/resources", async (req, res) => {
  const { name, context, ipRange, location } = req.body;

  if (!schemas.optionals(req.body)) {
    res.status(httpStatus.FORBIDDEN);
    res.json("both ipRange and location are unspecified");
    return;
  }

  if (!schemas.general(req.body)) {
    res.status(httpStatus.BAD_REQUEST);
    res.json("bad request");
    return;
  }

  if (location && !isValidTimeZone(location)) {
    res.status(httpStatus.BAD_REQUEST);
    res.json("bad request");
    return;
  }

  const collection = await getCollection();

  const { result } = await collection.insertOne({
    name,
    context,
    ipRange,
    location
  });

  if (!result.ok) {
    res.status(httpStatus.INTERNAL_SERVER_ERROR);
    res.json("could not insert context to database");
    return;
  }

  res.status(httpStatus.CREATED);
  res.json("created");
});

app.listen(port, () => console.log(`ACM listening on port ${port}!`));

const ipAndLocationAllowedForContext = async (ip, contextWrapper) => {
  if (contextWrapper.ipRange && !ipRangeCheck(ip, contextWrapper.ipRange)) {
    return false;
  }
  if (contextWrapper.location) {
    const lookupResult = (await lookup).get(ip);
    if (!lookupResult) {
      return false;
    }
    const { location } = lookupResult;
    const timeZone = location["time_zone"];
    if (contextWrapper.location !== timeZone) {
      return false;
    }
  }
  return true;
};

const isValidTimeZone = timeZone => {
  try {
    Intl.DateTimeFormat(undefined, { timeZone });
    return true;
  } catch (exception) {
    return false;
  }
};

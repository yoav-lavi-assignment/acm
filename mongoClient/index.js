const MongoClient = require("mongodb").MongoClient;
const {
  mongodb: { url, dbName }
} = require("../config");

const client = new MongoClient(url, { useNewUrlParser: true });

const connect = async () =>
  await new Promise((resolve, reject) =>
    client.connect(error => {
      if (error) {
        console.error(error);
        reject(error);
      }
      resolve(client.db(dbName));
    })
  );

module.exports = async () => await connect();

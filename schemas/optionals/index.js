const Joi = require("@hapi/joi");
const validator = require("../validator");

const schema = Joi.object().or("ipRange", "location");

module.exports = body => validator(body, schema);

const Joi = require("@hapi/joi");
const validator = require("../validator");

const schema = Joi.object().keys({
  name: Joi.string()
    .alphanum()
    .required(),
  context: Joi.string().required(),
  ipRange: Joi.array()
    .items(
      Joi.string()
        .ip({
          version: ["ipv4"],
          cidr: "required"
        })
        .required()
    )
    .optional(),
  location: Joi.string().optional()
});

module.exports = body => validator(body, schema);

module.exports = {
  general: require("./general"),
  optionals: require("./optionals"),
  validator: require("./validator")
};

const Joi = require("@hapi/joi");

module.exports = (body, schema) => {
  const result = Joi.validate(body, schema);
  return result.error === null;
};
